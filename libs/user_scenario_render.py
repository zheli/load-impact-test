# coding=utf-8
LINE_ENDING = ',\n'


class UserScenarioRender(object):
    REQUEST_BATCH_TEMPLATE = """http.request_batch({{
{requests}
}})
"""
    CLIENT_SLEEP_BATCH_TEMPLATE = 'client.sleep({interval})'

    def __init__(self, test_requests=None):
        self.test_requests = test_requests

    def get_content(self, batch_size=0, interval=None):
        content = ''
        request_batch = []

        for count, test_request in enumerate(self.test_requests):
            request_batch.append('{{"{}", "{}"}}'.format(test_request.method,
                                                         test_request.full_url))
            if batch_size > 0 and (count+1) % batch_size == 0:
                content = self._append_request_batch_to_content(content, request_batch, interval)
                request_batch = []

        content = self._append_request_batch_to_content(content, request_batch, interval)

        return self._strip_leading_newline(content)

    def _append_request_batch_to_content(self, content, request_batch, interval):
        if not request_batch:
            return content
        else:
            new_requests = self._strip_trailing_comma_and_newline(str.join(LINE_ENDING,
                                                                           request_batch))
            new_content = self.REQUEST_BATCH_TEMPLATE.format(requests=new_requests)

            if not content:
                return new_content
            else:
                if interval:
                    new_content = str.join('\n', [
                        self.CLIENT_SLEEP_BATCH_TEMPLATE.format(interval=interval), new_content])

                return str.join('', [content, new_content])

    @staticmethod
    def _strip_trailing_comma_and_newline(string):
        return str.rstrip(string, LINE_ENDING)

    @staticmethod
    def _strip_leading_newline(string):
        return str.lstrip(string, '\n')
