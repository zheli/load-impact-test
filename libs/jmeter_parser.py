# coding=utf-8
import urllib

import loadimpact

from bs4 import BeautifulSoup


class JmeterTestRequest(object):

    def __init__(self, domain, http_sampler_proxy=None):
        self.domain = domain
        self.path = _get_string_property_value_by_name(http_sampler_proxy, 'HTTPSampler.path')
        self.method = _get_string_property_value_by_name(http_sampler_proxy, 'HTTPSampler.method')
        self.params = _get_params_from_http_sampler_proxy(http_sampler_proxy)

    @property
    def full_url(self):
        return 'http://{}{}'.format(self.domain, self.get_path_with_params(self.path, self.params))

    @staticmethod
    def get_path_with_params(path, params):
        if params:
            return '{}?{}'.format(path, params)
        else:
            return '' if path is '/' else path


class JmeterParser(object):

    def __init__(self, stream):
        self.soup = BeautifulSoup(stream, 'lxml-xml')

    @property
    def testname(self):
        return self.soup.jmeterTestPlan.TestPlan.get('testname')

    @property
    def number_threads(self):
        return int(_get_string_property_value_by_name(self.soup.ThreadGroup,
                                                      'ThreadGroup.num_threads'))

    @property
    def ramp_time(self):
        return int(_get_string_property_value_by_name(self.soup.ThreadGroup,
                                                      'ThreadGroup.ramp_time'))

    @property
    def domain(self):
        return _get_string_property_value_by_name(self.soup.ConfigTestElement, 'HTTPSampler.domain')

    def get_test_requests(self):
        http_sampler_proxies = self.soup.findAll('HTTPSamplerProxy')
        for http_sampler_proxy in http_sampler_proxies:
            yield JmeterTestRequest(self.domain, http_sampler_proxy)

    def generate_test_config(self, user_scenario):
        return {
            'name': self.testname,
            'url': 'http://{}/'.format(self.domain),
            'config': {
                "user_type": "sbu",
                "load_schedule": [
                    {
                        "users": self.number_threads,
                        "duration": self.ramp_time
                    }
                ],
                "tracks": [{
                    "clips": [{
                        "user_scenario_id": user_scenario.id, "percent": 100
                    }],
                    "loadzone": loadimpact.LoadZone.AMAZON_US_ASHBURN
                }]
            }
        }


def _get_string_property_value_by_name(tag, name):
    if tag:
        return tag.find('stringProp', {'name': name}).string
    return None


def _get_argument_dict_from_element(element):
    return {
        _get_string_property_value_by_name(element, 'Argument.name'):
        _get_string_property_value_by_name(element, 'Argument.value')
    }


def _get_params_from_http_sampler_proxy(http_sampler_proxy):
    elements = http_sampler_proxy.find('collectionProp', {'name': 'Arguments.arguments'})

    if not elements:
        return None
    else:
        elements = elements.findAll('elementProp')
        arguments = {}
        [arguments.update(_get_argument_dict_from_element(element)) for element in elements]
        return urllib.urlencode(arguments)
