#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import time

import loadimpact

from libs.jmeter_parser import JmeterParser
from libs.user_scenario_render import UserScenarioRender


def main(filename):
    client = loadimpact.ApiTokenClient(
        api_token='1bbf3d10ee5c6d55d0e1d17c236278b32fbcc772cfcc923cc17c42236fdca62a')
    print('Importing test configuration from {}'.format(filename))
    with open(filename) as jmeter_file:
        parser = JmeterParser(jmeter_file)
        render = UserScenarioRender(parser.get_test_requests())
        load_script = render.get_content(batch_size=2, interval=15)

        print('Creating new user scenario')
        user_scenario = client.create_user_scenario({
            'name': "User scenario for '{}'".format(parser.testname),
            'load_script': load_script
        })
        print('User scenario created, id {}'.format(user_scenario.id))

        print('Creating new test configuration')
        test_config = client.create_test_config(parser.generate_test_config(user_scenario))
        print('New test configuration created, id {}'.format(test_config.id))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run Jmeter test with LoadImpact')
    parser.add_argument('--file', action='store', dest='filename', help='Jmeter file')
    arguments = parser.parse_args()
    main(arguments.filename)
