# coding=utf-8
import nose.tools as ns
from libs.jmeter_parser import JmeterTestRequest
from libs.user_scenario_render import UserScenarioRender


class MockJmeterTestRequest(JmeterTestRequest):
    path = None
    method = None
    params = None

    def __init__(self):
        pass


class TestUserScenarioRender(object):

    def test_render_request_batch_with_one_request(self):
        test_request = MockJmeterTestRequest()
        test_request.domain = 'test.loadimpact.com'
        test_request.path = '/'
        test_request.method = 'GET'
        test_request.params = None
        test_requests = [test_request]
        expected_result = """http.request_batch({
{"GET", "http://test.loadimpact.com"}
})
"""

        content = UserScenarioRender(test_requests=test_requests).get_content()
        ns.eq_(content, expected_result)

    def test_render_request_batch_with_two_requests(self):
        test_request = MockJmeterTestRequest()
        test_request.domain = 'test.loadimpact.com'
        test_request.path = '/'
        test_request.method = 'GET'
        test_request.params = None
        test_requests = [test_request, test_request]
        expected_result = """http.request_batch({
{"GET", "http://test.loadimpact.com"},
{"GET", "http://test.loadimpact.com"}
})
"""

        content = UserScenarioRender(test_requests=test_requests).get_content()
        ns.eq_(content, expected_result)

    def test_render_request_batch_with_four_requests_15s_interval_two_requests_in_a_batch(self):
        test_request = MockJmeterTestRequest()
        test_request.domain = 'test.loadimpact.com'
        test_request.path = '/'
        test_request.method = 'GET'
        test_request.params = None
        test_requests = [test_request, test_request, test_request, test_request]
        expected_result = """http.request_batch({
{"GET", "http://test.loadimpact.com"},
{"GET", "http://test.loadimpact.com"}
})
client.sleep(15)
http.request_batch({
{"GET", "http://test.loadimpact.com"},
{"GET", "http://test.loadimpact.com"}
})
"""

        content = UserScenarioRender(test_requests=test_requests).get_content(batch_size=2, interval=15)
        ns.eq_(content, expected_result)

    def test_render_request_batch_with_five_requests_15s_interval_two_requests_in_a_batch(self):
        test_request = MockJmeterTestRequest()
        test_request.domain = 'test.loadimpact.com'
        test_request.path = '/'
        test_request.method = 'GET'
        test_request.params = None
        test_requests = [test_request, test_request, test_request, test_request, test_request]
        expected_result = """http.request_batch({
{"GET", "http://test.loadimpact.com"},
{"GET", "http://test.loadimpact.com"}
})
client.sleep(15)
http.request_batch({
{"GET", "http://test.loadimpact.com"},
{"GET", "http://test.loadimpact.com"}
})
client.sleep(15)
http.request_batch({
{"GET", "http://test.loadimpact.com"}
})
"""

        content = UserScenarioRender(test_requests=test_requests).get_content(batch_size=2, interval=15)
        ns.eq_(content, expected_result)
