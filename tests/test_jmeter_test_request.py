# coding=utf-8
import nose.tools as ns
from libs.jmeter_parser import JmeterTestRequest


class TestJmeterTestRequest(object):

    def test_get_full_path_root_without_param(self):
        ns.eq_(JmeterTestRequest.get_path_with_params('/', None), '')

    def test_get_full_path_root_with_empty_param(self):
        ns.eq_(JmeterTestRequest.get_path_with_params('/', ''), '')

    def test_get_full_path_root_with_param(self):
        ns.eq_(JmeterTestRequest.get_path_with_params('/', 'bet=head'), '/?bet=head')

    def test_get_full_path_non_root_without_param(self):
        ns.eq_(JmeterTestRequest.get_path_with_params('/flip', None), '/flip')

    def test_get_full_path_non_root_with_param(self):
        ns.eq_(JmeterTestRequest.get_path_with_params('/flip', 'bet=head'), '/flip?bet=head')
