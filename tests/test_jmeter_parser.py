# coding=utf-8
import nose.tools as ns
from libs.jmeter_parser import JmeterParser

BASE_TEST_STRING = """
<?xml version="1.0" encoding="UTF-8"?>
<jmeterTestPlan version="1.2" properties="2.3" jmeter="2.8.20130705">
<hashTree>
<TestPlan guiclass="TestPlanGui" testclass="TestPlan"
testname="My test plan" enabled="true"></TestPlan>
<hashTree>
%s
</hashTree>
</hashTree>
</jmeterTestPlan>
"""


class TestJmeterParser(object):

    def test_parse_testname(self):
        test_stream = BASE_TEST_STRING % None
        parser = JmeterParser(test_stream)
        ns.eq_(parser.testname, 'My test plan')

    def test_parse_thread_group_attributes(self):
        test_stream = BASE_TEST_STRING % """
<ThreadGroup guiclass="ThreadGroupGui" testclass="ThreadGroup" testname="Thread Group" enabled="true">
<stringProp name="ThreadGroup.on_sample_error">continue</stringProp>
<elementProp name="ThreadGroup.main_controller" elementType="LoopController" guiclass="LoopControlPanel" testclass="LoopController" testname="Loop Controller" enabled="true">
<boolProp name="LoopController.continue_forever">false</boolProp>
<intProp name="LoopController.loops">-1</intProp>
</elementProp>
<stringProp name="ThreadGroup.num_threads">50</stringProp>
<stringProp name="ThreadGroup.ramp_time">60</stringProp>
<longProp name="ThreadGroup.start_time">1416235806000</longProp>
<longProp name="ThreadGroup.end_time">1416235806000</longProp>
<boolProp name="ThreadGroup.scheduler">false</boolProp>
<stringProp name="ThreadGroup.duration"></stringProp>
<stringProp name="ThreadGroup.delay"></stringProp>
</ThreadGroup>
        """
        parser = JmeterParser(test_stream)
        ns.eq_(parser.number_threads, 50)
        ns.eq_(parser.ramp_time, 60)

    def test_parse_config_test_elements(self):
        test_stream = BASE_TEST_STRING % """
<ThreadGroup guiclass="ThreadGroupGui" testclass="ThreadGroup" testname="Thread Group" enabled="true">
</ThreadGroup>
<hashTree>
<ConfigTestElement guiclass="HttpDefaultsGui" testclass="ConfigTestElement" testname="HTTP Request Defaults" enabled="true">
<stringProp name="HTTPSampler.domain">test.loadimpact.com</stringProp>
<stringProp name="HTTPSampler.port"></stringProp>
</ConfigTestElement>
<hashTree/>
</hashTree>
        """
        parser = JmeterParser(test_stream)
        ns.eq_(parser.domain, 'test.loadimpact.com')

    def test_parse_http_sampler_proxies(self):
        test_stream = BASE_TEST_STRING % """
<ThreadGroup guiclass="ThreadGroupGui" testclass="ThreadGroup" testname="Thread Group" enabled="true">
</ThreadGroup>
<hashTree>
<ConfigTestElement guiclass="HttpDefaultsGui" testclass="ConfigTestElement" testname="HTTP Request Defaults" enabled="true">
<stringProp name="HTTPSampler.domain">test.loadimpact.com</stringProp>
<stringProp name="HTTPSampler.port"></stringProp>
</ConfigTestElement>
<hashTree/>
<hashTree>
<HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" testname="/" enabled="true">
<stringProp name="HTTPSampler.path">/</stringProp>
<stringProp name="HTTPSampler.method">GET</stringProp>
</HTTPSamplerProxy>
<hashTree>
<HeaderManager guiclass="HeaderPanel" testclass="HeaderManager" testname="HTTP Header Manager" enabled="true">
</HeaderManager>
<hashTree/>
</hashTree>
<HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" testname="/news.php" enabled="true">
<stringProp name="HTTPSampler.path">/news.php</stringProp>
<stringProp name="HTTPSampler.method">GET</stringProp>
</HTTPSamplerProxy>
<hashTree>
<HeaderManager guiclass="HeaderPanel" testclass="HeaderManager" testname="HTTP Header Manager" enabled="true">
</HeaderManager>
<hashTree/>
</hashTree>
</hashTree>
        """
        parser = JmeterParser(test_stream)
        test_requests = [test_request for test_request in parser.get_test_requests()]
        ns.eq_(len(test_requests), 2)
        ns.eq_([test_request.path for test_request in test_requests], ['/', '/news.php'])
        ns.eq_([test_request.method for test_request in test_requests], ['GET', 'GET'])

    def test_parse_http_sampler_proxy_arguments(self):
        test_stream = BASE_TEST_STRING % """
<ThreadGroup guiclass="ThreadGroupGui" testclass="ThreadGroup" testname="Thread Group" enabled="true">
</ThreadGroup>
<hashTree>
<ConfigTestElement guiclass="HttpDefaultsGui" testclass="ConfigTestElement" testname="HTTP Request Defaults" enabled="true">
<stringProp name="HTTPSampler.domain">test.loadimpact.com</stringProp>
<stringProp name="HTTPSampler.port"></stringProp>
</ConfigTestElement>
<hashTree/>
<hashTree>
<HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" testname="/" enabled="true">
<elementProp name="HTTPsampler.Arguments" elementType="Arguments" guiclass="HTTPArgumentsPanel" testclass="Arguments" enabled="true">
<collectionProp name="Arguments.arguments">
<elementProp name="bet" elementType="HTTPArgument">
<stringProp name="Argument.name">bet</stringProp>
<stringProp name="Argument.value">heads</stringProp>
</elementProp>
</collectionProp>
</elementProp>
<stringProp name="HTTPSampler.path">/</stringProp>
<stringProp name="HTTPSampler.method">GET</stringProp>
</HTTPSamplerProxy>
<hashTree>
<HeaderManager guiclass="HeaderPanel" testclass="HeaderManager" testname="HTTP Header Manager" enabled="true">
</HeaderManager>
<hashTree/>
</hashTree>
</hashTree>
        """
        parser = JmeterParser(test_stream)
        test_request = parser.get_test_requests().next()
        ns.eq_(test_request.path, '/')
        ns.eq_(test_request.method, 'GET')
        ns.eq_(test_request.params, 'bet=heads')
