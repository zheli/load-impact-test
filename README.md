Bootstrap
====
To bootstrap the virtual environment, run bootstrap.sh script.
```
./bootstrap.sh
```
Then you can activate the virtual environment using 
```
source .virtualenv/bin/activate
```

Use the script
====
```
./upload_jmeter_test --file [your jmeter file]
```
There is a test jmeter file in the test_files folder, try it with:
```
./upload_jmeter_test --file test_files/test.jmx
```

Run unittest
====
Run unittest with nose
```
nosetests
```